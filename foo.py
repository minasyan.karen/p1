class Foo3:
    a = 2

    def foo(self):
        print(self.a)
        print(self.a)


class Foo5:
    a = 2
    b = 2

    @classmethod
    def stupid_multiply(cls):
        print(cls.a * cls.b)
        return 0+1
